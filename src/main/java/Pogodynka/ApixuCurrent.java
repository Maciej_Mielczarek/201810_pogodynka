package Pogodynka;

class ApixuCurrent
{
    private APLocation location;
    private APCurrent current;

    public ApixuCurrent() {
    }

    APLocation getLocation() {
        return location;
    }

    void setLocation(APLocation location) {
        this.location = location;
    }

    APCurrent getCurrent() {
        return current;
    }

    void setCurrent(APCurrent current) {
        this.current = current;
    }

    @Override
    public String toString() {
        return "ApixuCurrent{" +
                "location=" + location +
                ", current=" + current +
                '}';
    }
}

class APLocation
{
    private String name;
    private String region;
    private String country;
    private double lat;
    private double lon;
    private String tz_id;
    private long localtime_epoch;
    private String localtime;

    public APLocation() {
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    String getRegion() {
        return region;
    }

    void setRegion(String region) {
        this.region = region;
    }

    String getCountry() {
        return country;
    }

    void setCountry(String country) {
        this.country = country;
    }

    double getLat() {
        return lat;
    }

    void setLat(double lat) {
        this.lat = lat;
    }

    double getLon() {
        return lon;
    }

    void setLon(double lon) {
        this.lon = lon;
    }

    String getTz_id() {
        return tz_id;
    }

    void setTz_id(String tz_id) {
        this.tz_id = tz_id;
    }

    long getLocaltime_epoch() {
        return localtime_epoch;
    }

    void setLocaltime_epoch(long localtime_epoch) {
        this.localtime_epoch = localtime_epoch;
    }

    String getLocaltime() {
        return localtime;
    }

    void setLocaltime(String localtime) {
        this.localtime = localtime;
    }

    @Override
    public String toString() {
        return "APLocation{" +
                "name='" + name + '\'' +
                ", region='" + region + '\'' +
                ", country='" + country + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                ", tz_id='" + tz_id + '\'' +
                ", localtime_epoch=" + localtime_epoch +
                ", localtime='" + localtime + '\'' +
                '}';
    }
}

class APCurrent
{
    private long last_updated_epoch;
    private String last_updated;
    private double temp_c;
    private double temp_f;
    private int is_day;
    private APCondition condition;
    private double wind_mph;
    private double wind_kph;
    private double wind_degree;
    private String wind_dir;
    private double pressure_mb;
    private double pressure_in;
    private double precip_mm;
    private double precip_in;
    private double humidity;
    private double cloud;
    private double feelslike_c;
    private double feelslike_f;
    private double vis_km;
    private double vis_miles;

    public APCurrent() {
    }

    long getLast_updated_epoch() {
        return last_updated_epoch;
    }

    void setLast_updated_epoch(long last_updated_epoch) {
        this.last_updated_epoch = last_updated_epoch;
    }

    String getLast_updated() {
        return last_updated;
    }

    void setLast_updated(String last_updated) {
        this.last_updated = last_updated;
    }

    double getTemp_c() {
        return temp_c;
    }

    void setTemp_c(double temp_c) {
        this.temp_c = temp_c;
    }

    double getTemp_f() {
        return temp_f;
    }

    void setTemp_f(double temp_f) {
        this.temp_f = temp_f;
    }

    int getIs_day() {
        return is_day;
    }

    void setIs_day(int is_day) {
        this.is_day = is_day;
    }

    APCondition getCondition() {
        return condition;
    }

    void setCondition(APCondition condition) {
        this.condition = condition;
    }

    double getWind_mph() {
        return wind_mph;
    }

    void setWind_mph(double wind_mph) {
        this.wind_mph = wind_mph;
    }

    double getWind_kph() {
        return wind_kph;
    }

    void setWind_kph(double wind_kph) {
        this.wind_kph = wind_kph;
    }

    double getWind_degree() {
        return wind_degree;
    }

    void setWind_degree(double wind_degree) {
        this.wind_degree = wind_degree;
    }

    String getWind_dir() {
        return wind_dir;
    }

    void setWind_dir(String wind_dir) {
        this.wind_dir = wind_dir;
    }

    double getPressure_mb() {
        return pressure_mb;
    }

    void setPressure_mb(double pressure_mb) {
        this.pressure_mb = pressure_mb;
    }

    double getPressure_in() {
        return pressure_in;
    }

    void setPressure_in(double pressure_in) {
        this.pressure_in = pressure_in;
    }

    double getPrecip_mm() {
        return precip_mm;
    }

    void setPrecip_mm(double precip_mm) {
        this.precip_mm = precip_mm;
    }

    double getPrecip_in() {
        return precip_in;
    }

    void setPrecip_in(double precip_in) {
        this.precip_in = precip_in;
    }

    double getHumidity() {
        return humidity;
    }

    void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    double getCloud() {
        return cloud;
    }

    void setCloud(double cloud) {
        this.cloud = cloud;
    }

    double getFeelslike_c() {
        return feelslike_c;
    }

    void setFeelslike_c(double feelslike_c) {
        this.feelslike_c = feelslike_c;
    }

    double getFeelslike_f() {
        return feelslike_f;
    }

    void setFeelslike_f(double feelslike_f) {
        this.feelslike_f = feelslike_f;
    }

    double getVis_km() {
        return vis_km;
    }

    void setVis_km(double vis_km) {
        this.vis_km = vis_km;
    }

    double getVis_miles() {
        return vis_miles;
    }

    void setVis_miles(double vis_miles) {
        this.vis_miles = vis_miles;
    }

    @Override
    public String toString() {
        return "APCurrent{" +
                "last_updated_epoch=" + last_updated_epoch +
                ", last_updated='" + last_updated + '\'' +
                ", temp_c=" + temp_c +
                ", temp_f=" + temp_f +
                ", is_day=" + is_day +
                ", condition=" + condition +
                ", wind_mph=" + wind_mph +
                ", wind_kph=" + wind_kph +
                ", wind_degree=" + wind_degree +
                ", wind_dir='" + wind_dir + '\'' +
                ", pressure_mb=" + pressure_mb +
                ", pressure_in=" + pressure_in +
                ", precip_mm=" + precip_mm +
                ", precip_in=" + precip_in +
                ", humidity=" + humidity +
                ", cloud=" + cloud +
                ", feelslike_c=" + feelslike_c +
                ", feelslike_f=" + feelslike_f +
                ", vis_km=" + vis_km +
                ", vis_miles=" + vis_miles +
                '}';
    }
}

class APCondition
{
    private String text;
    private String icon;
    private double code;

    public APCondition() {
    }

    String getText() {
        return text;
    }

    void setText(String text) {
        this.text = text;
    }

    String getIcon() {
        return icon;
    }

    void setIcon(String icon) {
        this.icon = icon;
    }

    double getCode() {
        return code;
    }

    void setCode(double code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "APCondition{" +
                "text='" + text + '\'' +
                ", icon='" + icon + '\'' +
                ", code=" + code +
                '}';
    }
}