package Pogodynka;

class WeatherServicesUrls {
    private static final String[] UrlBases = {"http://api.openweathermap.org/data/2.5/", "http://api.apixu.com/v1/"};
    private static final String[] UrlCurrentParts = {"weather?", "current.json?"};
    private static String[] UrlForecastParts = {};
    private static final String[] UrlCityParts = {"q=", "q="};
    private static final String[] UrlCoordParts = {"lat=", "q=", "&lon=", ","};
    @SuppressWarnings("SpellCheckingInspection")
    private static final String[] UrlKeys = {"&appid=e98ec0b55eff87bed3170a3ba71bd2b5", "key=74debbafc7f84884840122104182809&"};
    private static final int DefaultNDays = 1;
    private static final int MaxNDays = 5;

    static String getUrl(int id, String CityName, int NDays)
    {
        if(NDays==0)
        {
            if(id==0)
            {
                return UrlBases[0] + UrlCurrentParts[0] + UrlCityParts[0] + CityName + UrlKeys[0];
            }
            else if(id==1)
            {
                return UrlBases[1]+UrlCurrentParts[1]+UrlKeys[1] +UrlCityParts[1] + CityName;
            }
        }
        else if(NDays>0 && NDays<=MaxNDays)
        {
            if(id==0)
            {
                return "";
            }
            else if(id==1)
            {
                return "";
            }
        }
        return "";
    }
    public static String getUrl(int id, String CityName)
    {
        return getUrl(id,CityName,DefaultNDays);
    }

    static String getUrl(int id, double Latitude, double Longitude, int NDays)
    {
        if(NDays==0)
        {
            if(id==0)
            {
                return UrlBases[0]+UrlCurrentParts[0]+UrlCoordParts[0] +Latitude + UrlCoordParts[2]+Longitude+ UrlKeys[0];
            }
            else if(id==1)
            {
                return UrlBases[1]+UrlCurrentParts[1]+UrlKeys[1] + UrlCoordParts[1]+Latitude+UrlCoordParts[3]+Longitude;
            }
        }
        else if(NDays>0 && NDays<=MaxNDays)
        {
            return "";
        }
        return "";
    }
    public static String getUrl(int id, double Latitude, double Longitude)
    {
        return getUrl(id, Latitude, Longitude,DefaultNDays);
    }

}

