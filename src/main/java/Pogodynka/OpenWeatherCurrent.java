package Pogodynka;

import com.fasterxml.jackson.annotation.*;

class OpenWeatherCurrent
{
    private OWCoordinates coord;
    private OWWeather[] weather;
    private String base;
    private OWMain main;
    private OWWind wind;
    private OWClouds clouds;
    private OWRain rain;
    private OWSnow snow;
    private long dt;
    private OWSys sys;
    private long id;
    private String name;
    private int cod;

    public OpenWeatherCurrent() {
    }

    OWCoordinates getCoord() {
        return coord;
    }

    void setCoord(OWCoordinates coord) {
        this.coord = coord;
    }

    OWWeather[] getWeather() {
        return weather;
    }

    void setWeather(OWWeather[] weather) {
        this.weather = weather;
    }

    String getBase() {
        return base;
    }

    void setBase(String base) {
        this.base = base;
    }

    OWMain getMain() {
        return main;
    }

    void setMain(OWMain main) {
        this.main = main;
    }

    OWWind getWind() {
        return wind;
    }

    void setWind(OWWind wind) {
        this.wind = wind;
    }

    OWClouds getClouds() {
        return clouds;
    }

    void setClouds(OWClouds clouds) {
        this.clouds = clouds;
    }

    OWRain getRain() {
        return rain;
    }

    void setRain(OWRain rain) {
        this.rain = rain;
    }

    OWSnow getSnow() {
        return snow;
    }

    void setSnow(OWSnow snow) {
        this.snow = snow;
    }

    long getDt() {
        return dt;
    }

    void setDt(long dt) {
        this.dt = dt;
    }

    OWSys getSys() {
        return sys;
    }

    void setSys(OWSys sys) {
        this.sys = sys;
    }

    long getId() {
        return id;
    }

    void setId(long id) {
        this.id = id;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    int getCod() {
        return cod;
    }

    void setCod(int cod) {
        this.cod = cod;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "OpenWeatherCurrent{" +
                "coord=" + coord +
                ", weather=" + java.util.Arrays.toString(weather) +
                ", base='" + base + '\'' +
                ", main=" + main +
                ", wind=" + wind +
                ", clouds=" + clouds +
                ", rain=" + rain +
                ", snow=" + snow +
                ", dt=" + dt +
                ", sys=" + sys +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", cod=" + cod +
                '}';
    }
}

class OWCoordinates
{
    private double lon;
    private double lat;

    public OWCoordinates() {
    }

    double getLon() {
        return lon;
    }

    void setLon(double lon) {
        this.lon = lon;
    }

    double getLat() {
        return lat;
    }

    void setLat(double lat) {
        this.lat = lat;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "OWCoordinates{" +
                "lon=" + lon +
                ", lat=" + lat +
                '}';
    }
}

class OWWeather
{
    private int id;
    private String main;
    private String description;
    private String icon;

    public OWWeather() {
    }

    int getId() {
        return id;
    }

    void setId(int id) {
        this.id = id;
    }

    String getMain() {
        return main;
    }

    void setMain(String main) {
        this.main = main;
    }

    String getDescription() {
        return description;
    }

    void setDescription(String description) {
        this.description = description;
    }

    String getIcon() {
        return icon;
    }

    void setIcon(String icon) {
        this.icon = icon;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "OWWeather{" +
                "id=" + id +
                ", main='" + main + '\'' +
                ", description='" + description + '\'' +
                ", icon='" + icon + '\'' +
                '}';
    }
}

class OWMain
{
    private double temp;
    private double pressure;
    private double humidity;
    private double temp_min;
    private double temp_max;

    public OWMain() {
    }

    double getTemp() {
        return temp;
    }

    void setTemp(double temp) {
        this.temp = temp;
    }

    double getPressure() {
        return pressure;
    }

    void setPressure(double pressure) {
        this.pressure = pressure;
    }

    double getHumidity() {
        return humidity;
    }

    void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    double getTemp_min() {
        return temp_min;
    }

    void setTemp_min(double temp_min) {
        this.temp_min = temp_min;
    }

    double getTemp_max() {
        return temp_max;
    }

    void setTemp_max(double temp_max) {
        this.temp_max = temp_max;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "OWMain{" +
                "temp=" + temp +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                ", temp_min=" + temp_min +
                ", temp_max=" + temp_max +
                '}';
    }
}

class OWWind
{
    private double speed;
    private double deg;

    public OWWind() {
    }

    double getSpeed() {
        return speed;
    }

    void setSpeed(double speed) {
        this.speed = speed;
    }

    double getDeg() {
        return deg;
    }

    void setDeg(double deg) {
        this.deg = deg;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "OWWind{" +
                "speed=" + speed +
                ", deg=" + deg +
                '}';
    }
}

class OWClouds
{
    private double all;

    public OWClouds() {
    }

    double getAll() {
        return all;
    }

    void setAll(double all) {
        this.all = all;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "OWClouds{" +
                "all=" + all +
                '}';
    }
}

class OWRain
{
    private double threeh;

    public OWRain() {
    }

    @JsonProperty("3h")
    double getThreeh() {
        return threeh;
    }

    @JsonProperty("3h")
    void setThreeh(double threeh) {
        this.threeh = threeh;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "OWRain{" +
                "3h=" + threeh +
                '}';
    }
}

class OWSnow
{
    private double threeh;

    public OWSnow() {
    }

    @JsonProperty("3h")
    double getThreeh() {
        return threeh;
    }

    @JsonProperty("3h")
    void setThreeh(double threeh) {
        this.threeh = threeh;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "OWSnow{" +
                "3h=" + threeh +
                '}';
    }
}

class OWSys
{
    private int type;
    private int id;
    private double message;
    private String country;
    private long sunrise;
    private long sunset;

    public OWSys() {
    }

    int getType() {
        return type;
    }

    void setType(int type) {
        this.type = type;
    }

    int getId() {
        return id;
    }

    void setId(int id) {
        this.id = id;
    }

    double getMessage() {
        return message;
    }

    void setMessage(double message) {
        this.message = message;
    }

    String getCountry() {
        return country;
    }

    void setCountry(String country) {
        this.country = country;
    }

    long getSunrise() {
        return sunrise;
    }

    void setSunrise(long sunrise) {
        this.sunrise = sunrise;
    }

    long getSunset() {
        return sunset;
    }

    void setSunset(long sunset) {
        this.sunset = sunset;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "OWSys{" +
                "type=" + type +
                ", id=" + id +
                ", message=" + message +
                ", country='" + country + '\'' +
                ", sunrise=" + sunrise +
                ", sunset=" + sunset +
                '}';
    }
}