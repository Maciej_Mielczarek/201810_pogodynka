package Pogodynka;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.sql.*;

@RestController
class ForecastController
{
    private void LogRequest(String Query, String Result)
    {
        long nowWithMs = System.currentTimeMillis();
        long now = nowWithMs / 1000L;
        int msnow = (int)(nowWithMs % 1000L);
        try
                (
                        Connection conn = DriverManager.getConnection(
                                "jdbc:mysql://localhost:3306/weather?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","myuser","xxxx");
                        Statement stmt = conn.createStatement()
                )
        {
            String logEntry = "INSERT into log values ("+(int)now+", "+msnow+", "+Query+", "+Result+");";
            stmt.execute(logEntry);
        }
        catch(SQLException ex) {
            ex.printStackTrace();
        }
    }

    private Forecast CalculateCurrentAverageWeatherConditions(OpenWeatherCurrent currentOWWeather, ApixuCurrent currentAPWeather)
    {
        Forecast currentAvgWeather = new Forecast();

        double avgTemperature = (currentOWWeather.getMain().getTemp()-273.15+currentAPWeather.getCurrent().getTemp_c())/2;
        currentAvgWeather.setTemperature(avgTemperature);

        double avgPressure = (currentOWWeather.getMain().getPressure()+currentAPWeather.getCurrent().getPressure_mb())/2;
        currentAvgWeather.setPressure(avgPressure);

        double avgHumidity = (currentOWWeather.getMain().getHumidity()+currentAPWeather.getCurrent().getHumidity())/2;
        currentAvgWeather.setHumidity(avgHumidity);

        int avgWindDir = (int)((currentOWWeather.getWind().getDeg()+currentAPWeather.getCurrent().getWind_degree())/2);
        double avgWindSpeed = (currentOWWeather.getWind().getSpeed()*3.6+currentAPWeather.getCurrent().getWind_kph())/2;
        currentAvgWeather.setWind(new Pogodynka.WindType(avgWindDir,avgWindSpeed));

        return currentAvgWeather;
    }

    private Forecast CalculateForecastAverageWeatherConditions(OpenWeatherForecast forecastOWWeather, ApixuForecast forecastApixuWeather)
    {
        Forecast forecastAvgWeather = null;
        return forecastAvgWeather;
    }

    @GetMapping("/cities/{CityName}/days/{NDays}")
    Forecast WeatherInCityInNDays(@PathVariable String CityName, @PathVariable int NDays)
    {
        String Query = "\"/cities/"+CityName+"/days/"+NDays+"\"";
        RestTemplate restTemplate = new RestTemplate();
        if(NDays==0)
        {
            OpenWeatherCurrent currentOWWeather = restTemplate.getForObject(WeatherServicesUrls.getUrl(0,CityName,NDays),OpenWeatherCurrent.class);
            ApixuCurrent currentAPWeather = restTemplate.getForObject(WeatherServicesUrls.getUrl(1,CityName,NDays),ApixuCurrent.class);
            Forecast currentAvgWeather = CalculateCurrentAverageWeatherConditions(currentOWWeather, currentAPWeather);
            LogRequest(Query,"\""+currentAvgWeather+"\"");
            return currentAvgWeather;
        }
        else if(NDays>0)
        {
            return null;
        }
        else
        {
            return null;
        }
    }

    @GetMapping("/cities/{CityName}")
    Forecast WeatherInCityTomorrow(@PathVariable String CityName)
    {
        return null;
    }

    @GetMapping("/lat/{Latitude}/lon/{Longitude}/days/{NDays}")
    Forecast WeatherAtLocationInNDays(@PathVariable double Latitude, @PathVariable double Longitude, @PathVariable int NDays)
    {
        RestTemplate restTemplate = new RestTemplate();
        if(NDays==0)
        {
            OpenWeatherCurrent currentOWWeather = restTemplate.getForObject(WeatherServicesUrls.getUrl(0,Latitude,Longitude,NDays),OpenWeatherCurrent.class);
            ApixuCurrent currentAPWeather = restTemplate.getForObject(WeatherServicesUrls.getUrl(1,Latitude,Longitude,NDays),ApixuCurrent.class);
            Forecast currentAvgWeather = CalculateCurrentAverageWeatherConditions(currentOWWeather, currentAPWeather);
            LogRequest("\"/lat/"+Latitude+"/lon/"+Longitude+"/days/"+NDays+"\"","\""+currentAvgWeather+"\"");
            return currentAvgWeather;
        }
        else if(NDays>0)
        {
            return null;
        }
        else
        {
            return null;
        }
    }

    @GetMapping("/lat/{Latitude}/lon/{Longitude}")
    Forecast WeatherAtLocationTomorrow(@PathVariable double Latitude, @PathVariable double Longitude)
    {
        return null;
    }
}