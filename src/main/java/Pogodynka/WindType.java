package Pogodynka;

class WindType
{
    private int Direction;
    private double SpeedKPH;

    public WindType(int direction, double speedKPH) {
        Direction = direction;
        SpeedKPH = speedKPH;
    }

    public WindType() {
    }

    public int getDirection() {
        return Direction;
    }

    void setDirection(int direction) {
        Direction = direction;
    }

    public double getSpeedKPH() {
        return SpeedKPH;
    }

    void setSpeedKPH(double speedKPH) {
        SpeedKPH = speedKPH;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "WindType{" +
                "Direction=" + Direction +
                ", SpeedKPH=" + SpeedKPH +
                '}';
    }
}