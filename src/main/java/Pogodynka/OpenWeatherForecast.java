package Pogodynka;

class OpenWeatherForecast
{
    private OWFList[] list;

    OWFList[] getList() {
        return list;
    }

    void setList(OWFList[] list) {
        this.list = list;
    }

    public OpenWeatherForecast() {
    }

    @Override
    public String toString() {
        return "OpenWeatherForecast{" +
                "list=" + list +
                '}';
    }
}

class OWFList
{
    private long dt;
    private OWFMain main;
    private OWFWind wind;

    OWFList() {
    }

    long getDt() {
        return dt;
    }

    void setDt(long dt) {
        this.dt = dt;
    }

    OWFMain getMain() {
        return main;
    }

    void setMain(OWFMain main) {
        this.main = main;
    }

    OWFWind getWind() {
        return wind;
    }

    void setWind(OWFWind wind) {
        this.wind = wind;
    }

    @Override
    public String toString() {
        return "OWFList{" +
                "dt=" + dt +
                ", main=" + main +
                ", wind=" + wind +
                '}';
    }
}

class OWFMain
{
    private double temp;
    private double pressure;
    private double humidity;

    OWFMain() {
    }

    double getTemp() {
        return temp;
    }

    void setTemp(double temp) {
        this.temp = temp;
    }

    double getPressure() {
        return pressure;
    }

    void setPressure(double pressure) {
        this.pressure = pressure;
    }

    double getHumidity() {
        return humidity;
    }

    void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    @Override
    public String toString() {
        return "OWFMain{" +
                "temp=" + temp +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                '}';
    }
}

class OWFWind
{
    private double speed;
    private double deg;

    OWFWind() {
    }

    double getSpeed() {
        return speed;
    }

    void setSpeed(double speed) {
        this.speed = speed;
    }

    double getDeg() {
        return deg;
    }

    void setDeg(double deg) {
        this.deg = deg;
    }

    @Override
    public String toString() {
        return "OWFWind{" +
                "speed=" + speed +
                ", deg=" + deg +
                '}';
    }
}