package Pogodynka;

import java.util.Arrays;

class ApixuForecast
{
    private APForecast forecast;

    public ApixuForecast() {
    }

    APForecast getForecast() {
        return forecast;
    }

    void setForecast(APForecast forecast) {
        this.forecast = forecast;
    }

    @Override
    public String toString() {
        return "ApixuForecast{" +
                "forecast=" + forecast +
                '}';
    }
}

class APForecast
{
    private APForecastDay[] forecastday;

    APForecastDay[] getForecastday() {
        return forecastday;
    }

    void setForecastday(APForecastDay[] forecastday) {
        this.forecastday = forecastday;
    }

    @Override
    public String toString() {
        return "APForecast{" +
                "forecastday=" + Arrays.toString(forecastday) +
                '}';
    }

    APForecast() {
    }
}

class APForecastDay
{
    private long dateEpoch;
    private APFDay day;

    APForecastDay() {
    }

    long getDateEpoch() {
        return dateEpoch;
    }

    void setDateEpoch(long dateEpoch) {
        this.dateEpoch = dateEpoch;
    }

    APFDay getDay() {
        return day;
    }

    void setDay(APFDay day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return "APForecastDay{" +
                "dateEpoch=" + dateEpoch +
                ", day=" + day +
                '}';
    }
}

class APFDay
{
    private double avgtempC;
    private double maxwindKph;
    private double avghumidity;

    APFDay() {
    }

    double getAvgtempC() {
        return avgtempC;
    }

    void setAvgtempC(double avgtempC) {
        this.avgtempC = avgtempC;
    }

    double getMaxwindKph() {
        return maxwindKph;
    }

    void setMaxwindKph(double maxwindKph) {
        this.maxwindKph = maxwindKph;
    }

    double getAvghumidity() {
        return avghumidity;
    }

    void setAvghumidity(double avghumidity) {
        this.avghumidity = avghumidity;
    }

    @Override
    public String toString() {
        return "APFDay{" +
                "avgtempC=" + avgtempC +
                ", maxwindKph=" + maxwindKph +
                ", avghumidity=" + avghumidity +
                '}';
    }
}