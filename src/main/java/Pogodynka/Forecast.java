package Pogodynka;

public class Forecast
{
    private double Temperature;
    private double Pressure;
    private double Humidity;
    private WindType Wind;

    public Forecast(double temperature, double pressure, double humidity, WindType wind) {
        Temperature = temperature;
        Pressure = pressure;
        Humidity = humidity;
        Wind = wind;
    }

    public Forecast() {
    }

    public double getTemperature() {
        return Temperature;
    }

    void setTemperature(double temperature) {
        Temperature = temperature;
    }

    public double getPressure() {
        return Pressure;
    }

    void setPressure(double pressure) {
        Pressure = pressure;
    }

    public double getHumidity() {
        return Humidity;
    }

    void setHumidity(double humidity) {
        Humidity = humidity;
    }

    public WindType getWind() {
        return Wind;
    }

    void setWind(WindType wind) {
        Wind = wind;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "Forecast{" +
                "Temperature=" + Temperature +
                ", Pressure=" + Pressure +
                ", Humidity=" + Humidity +
                ", Wind=" + Wind +
                '}';
    }
}

